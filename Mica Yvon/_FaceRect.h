// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FaceRect.h instead.

#import <CoreData/CoreData.h>


extern const struct FaceRectAttributes {
	__unsafe_unretained NSString *rect;
} FaceRectAttributes;

extern const struct FaceRectRelationships {
} FaceRectRelationships;

extern const struct FaceRectFetchedProperties {
} FaceRectFetchedProperties;




@interface FaceRectID : NSManagedObjectID {}
@end

@interface _FaceRect : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FaceRectID*)objectID;





@property (nonatomic, strong) NSString* rect;



//- (BOOL)validateRect:(id*)value_ error:(NSError**)error_;






@end

@interface _FaceRect (CoreDataGeneratedAccessors)

@end

@interface _FaceRect (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveRect;
- (void)setPrimitiveRect:(NSString*)value;




@end
