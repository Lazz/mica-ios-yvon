// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FaceRect.m instead.

#import "_FaceRect.h"

const struct FaceRectAttributes FaceRectAttributes = {
	.rect = @"rect",
};

const struct FaceRectRelationships FaceRectRelationships = {
};

const struct FaceRectFetchedProperties FaceRectFetchedProperties = {
};

@implementation FaceRectID
@end

@implementation _FaceRect

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FaceRect" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FaceRect";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FaceRect" inManagedObjectContext:moc_];
}

- (FaceRectID*)objectID {
	return (FaceRectID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic rect;











@end
